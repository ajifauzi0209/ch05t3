package com.kazakimaru.ch05t3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.kazakimaru.ch05t3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

//        withoutViewModel()
        withViewModel()
    }

    private fun withoutViewModel() {
        buttonPlusClicked()
        buttonMinusClicked()
    }

    private fun buttonPlusClicked() {
        binding.btnPlus.setOnClickListener {
            val newText = binding.tvCount.text.toString().toInt().plus(1)
            updateText(newText)
        }
    }

    private fun buttonMinusClicked() {
        binding.btnMinus.setOnClickListener {
            val newText = binding.tvCount.text.toString().toInt().minus(1)
            updateText(newText)
        }
    }

    private fun withViewModel() {
        binding.btnPlus.setOnClickListener {
            viewModel.increment()
        }

        binding.btnMinus.setOnClickListener {
            viewModel.decrement()
        }

        viewModel.nilaiBaru.observe(this) {
            updateText(it)
        }
    }

    private fun updateText(newValue: Int) {
        binding.tvCount.text = newValue.toString()
    }

}