package com.kazakimaru.ch05t3

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {
    val nilaiBaru = MutableLiveData(0)

    fun increment() {
        nilaiBaru.value = nilaiBaru.value?.plus(1)
    }

    fun decrement() {
        nilaiBaru.value = nilaiBaru.value?.minus(1)
    }
}